# SFML TEMPLATE for VISUAL STUDIO CODE #
####   By Anders Marstein Kruke ([andersmkruke.tech](https://andersmkruke.tech))    ####

## WHAT IS IT? ##
This is a template project for SFML, specifically intended for Visual Studio Code, to get started with minimal configuration. Code is compiled using the cl.exe compiler from Visual Studio Build Tools, and VS Code Tasks are set up to build and run the program. The program will run the example program from SFML's [Getting Started](https://www.sfml-dev.org/tutorials/2.4/start-vc.php) section.

## HOW TO: ##
1. Download [Visual Studio Build Tools](http://landinghub.visualstudio.com/visual-cpp-build-tools) or [Visual Studio IDE](https://www.visualstudio.com/).
2. In build.bat, specify the correct path to vsvarsall.bat and intended architecture (usually x64).
3. In build.bat, specify correct values for the user specified values (at the top).  
4. Copy required SFML .dll files into the root directory of the project.
5. In main.cpp, select the "#include ..." line and click the lightbulb create a configure file with SFML include path
6. You are now ready!

---
- To build, Run build task (default shortcut: Ctrl + Shift + B)
- To build and run in VS Code terminal open VS Code Command Palett (Default shortcut: Ctrl + Shift + P) and choose Tasks: Run Task > build&run
- To run, choose Debug>Start Debugging (Default shortcut: F5) or Debug>Start Without Debugging (Default shortcut: Ctrl + F5)

---
- To change remote origin, use command: git remote set-url \<URL\>
- Then use: git push -u origin master