:: User specified values ::
set target=.\target
set exe_name=program
set libs=c:\Users\amk\cpp\libs\SFML-2.4.2\lib
set sources=@sources
set includes=@includes
:: _____________________ ::
set exe=%target%\%exe_name%.exe


@echo off
IF NOT "%1"=="run" (
    echo -Build Only-
    GOTO BuildOnly
)

echo -Build and Run-

:BuildOnly
:: ** PATH MAY HAVE TO BE UPDATED **                                                                    ::
:: Path to VS Build Tools' vcvarsall with intended architecture (run without any value to list options) ::
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64

:: /Zi - Debugging                                                                  ::
:: /Fo - Objects folder (Must end in double backslash!)                             ::
:: /EH - Exception handling                                                         ::
::       sc - catches C++ exceptions only and tells the compiler to assume that     ::
::          functions declared as extern "C" never throw a C++ exception.           ::
set compilerflags=/EHsc /Zi /Fo:%target%\\

:: /LIBPATH - Path to libraries                 ::
:: /out - Output executable                     ::
set linkerflags=/LIBPATH:%libs% /out:%exe%

:: @sources - File specifying source files              ::
:: @includes - File specifying include folders          ::
cl.exe %compilerflags% %sources% %includes% /link %linkerflags%

echo Build Complete

IF NOT "%1"=="run" GOTO End

:Run
echo(
echo Running:
echo ...
%exe%

:End